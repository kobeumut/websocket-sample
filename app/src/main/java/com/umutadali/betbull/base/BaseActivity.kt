package com.umutadali.betbull.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import io.reactivex.disposables.Disposable

/**
 * This is the abstract class where you must inherit your new activities/
 * This class provides you simple clean architecture with lifecycle aware. DataBinding
 * auto include this base activity.
 *
 * @param T is your activity's view data binding class
 * for example you use the ActivityMainBinding class generated for activity_main.xml.
 * Whatever your layout is, you should use generated ViewDataBinding class.
 * Simple use: BaseActivity<ActivityMainBinding>()
 */
abstract class BaseActivity<T : ViewDataBinding> : AppCompatActivity(), LifecycleOwner {
    /**
     * LifecycleRegistry is variable for Activity's lifecycle for listening base processes.
     * Do not override unless absolutely necessary.
     */
    lateinit var lifecycleRegistry: LifecycleRegistry

    /**
     * binding gets to all layout properties for simple use
     */
    lateinit var binding: T
    private lateinit var disposeObservable: Disposable
    /**
     * Used for simple operation set layout with data binding
     *
     * @return
     */
    @LayoutRes
    abstract fun getLayoutResId(): Int

    /**
     * General onCreate Method is not required any more. Layout set with [getLayoutResId] function.
     * If you want to write some code in onCreate, you can override it. This function provides you set lifecycle,
     * data binding and layout.
     *
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleRegistry = LifecycleRegistry(this)
        binding = DataBindingUtil.setContentView(this, getLayoutResId())

    }

    /**
     * This function return lifecycleRegistry in [BaseActivity]. Dont override it. Only use.
     *
     * @return [lifecycleRegistry]
     */
    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }


}
