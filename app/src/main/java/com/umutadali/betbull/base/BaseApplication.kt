package com.umutadali.betbull.base

import android.util.Log
import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.umutadali.betbull.api.ApiInterface
import com.umutadali.betbull.helper.dialog.ProgressDialogModule
import com.umutadali.betbull.ui.viewModel.ListUserViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author Umut ADALI
 *
 * Base application is Application wrapper which extends MultiDexApplication because of 60K limits and using
 * dependency injection initialize.
 *
 */
class BaseApplication : MultiDexApplication() {
    private val progressModule = module {
        single { ProgressDialogModule() }
    }
    private val networkModule = module {

        factory<HttpLoggingInterceptor> {
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { Log.d("API", it) })
                .setLevel(HttpLoggingInterceptor.Level.HEADERS)
        }

        single {
            OkHttpClient.Builder()
                .addInterceptor(get<HttpLoggingInterceptor>())
                .addNetworkInterceptor(StethoInterceptor())
                .build()
        }

        single {
            Retrofit.Builder()
                .client(get())
                .baseUrl("https://my-json-server.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        factory { get<Retrofit>().create(ApiInterface::class.java) }
        viewModel {
            ListUserViewModel(get(),get())
        }
    }

    /**
     * Calls koin initialize function
     *
     */
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)

        startKoin {
            androidLogger()
            // use the Android context given there
            androidContext(this@BaseApplication)
            //Custom modules for application
            modules(listOf(progressModule, networkModule))
        }
    }

}