package com.umutadali.betbull.ui.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.umutadali.betbull.api.ApiInterface
import com.umutadali.betbull.helper.enqueue
import com.umutadali.betbull.model.ResponseData
import okhttp3.*


class ListUserViewModel(private val apiServices: ApiInterface, val okHttpClient: OkHttpClient) : ViewModel() {

    private val _listItems = MutableLiveData<ResponseData>()
    val listItems: LiveData<ResponseData>
        get() = _listItems
    private lateinit var websocket: WebSocket
    private val _textView = MutableLiveData<String>()
    val textView: LiveData<String>
        get() = _textView
    private val normalClosureStatus = 1000


    fun startSocket() {
        val request = Request.Builder().url("wss://javascript.info/article/websocket/chat/ws").build()
        val listener = object : WebSocketListener() {
            override fun onMessage(webSocket: WebSocket, text: String) {
                super.onMessage(webSocket, text)
                _textView.postValue(text)
            }
        }
        websocket = okHttpClient.newWebSocket(request, listener)
        websocket.request()
    }

    fun sendMessage(message: String) {
        websocket.send(message)
    }

    fun closeWebsocket() {
        websocket.close(normalClosureStatus, "Exit App")
    }

    fun getInformations(result:(isSuccess:Boolean)->Unit) {
        apiServices.getInformations().enqueue { result ->
            result.onFailure {
                Log.e("errorMock", it.localizedMessage)
                result(false)
            }
            result.onSuccess {
                _listItems.postValue(it)
                result(true)
            }
        }
    }

    fun updateItem(id: Int, newName: String) {
        val list = _listItems.value?.data?.toMutableList()
        list?.find { it.id == id }?.name = newName
        list?.let { _listItems.postValue(ResponseData(it)) }
    }
}