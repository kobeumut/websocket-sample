package com.umutadali.betbull.ui.view

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import com.link184.kidadapter.setUp
import com.link184.kidadapter.simple.SingleKidAdapter
import com.umutadali.betbull.R
import com.umutadali.betbull.base.BaseActivity
import com.umutadali.betbull.databinding.ActivityListUserBinding
import com.umutadali.betbull.helper.checkInteger
import com.umutadali.betbull.helper.listener.ProgressListener
import com.umutadali.betbull.model.ResponseObject
import com.umutadali.betbull.ui.viewModel.ListUserViewModel
import kotlinx.android.synthetic.main.content_list_user.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListUserActivity : BaseActivity<ActivityListUserBinding>(), ProgressListener {
    override fun getLayoutResId() = R.layout.activity_list_user
    private val viewModel: ListUserViewModel by viewModel()
    private var singleKidAdapter: SingleKidAdapter<ResponseObject>? = null
    private var user: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showProgress()
        getList()
        viewModel.startSocket()
        viewModel.getInformations {
            runOnUiThread {
                if (!it) {
                    Toast.makeText(this@ListUserActivity, "Not Connected, please try again later", Toast.LENGTH_LONG).show()
                }
                hideProgress()
            }
        }
        setUI()
    }

    private fun getList() {
        viewModel.listItems.observe(this, Observer { list ->
            singleKidAdapter = binding.listOfMock.setUp<ResponseObject> {
                withLayoutResId(R.layout.content_list_user)
                list?.let { l -> withItems(l.data) }
                bindIndexed { item, _ ->
                    mockId.text = "ID: ${item.id.toString()}"
                    mockName.text = item.name
                }
            }
        })
    }

    private fun setUI() {
        binding.sendButton.setOnClickListener {
            if (binding.messageInput.text.isNotEmpty()) {
                viewModel.sendMessage(binding.messageInput.text.toString())
                binding.messageInput.text.clear()
            }
        }
        viewModel.textView.observe(this, Observer { message ->
            val splittedText = message.split("-")
            val id = splittedText[0].trim()
            if (message.contains("-") && (id.checkInteger()) && !splittedText[1].trim().checkInteger()) {
                user = splittedText[1]
                user?.let { name ->
                    var oldName = ""
                    viewModel.listItems.value?.data?.find { it.id == id.toInt() }?.name?.let {
                        oldName = it
                    }

                    when {
                        name.toLowerCase() == "login" -> {
                            GlobalScope.launch(Dispatchers.Main) {
                                supportActionBar?.title = "$oldName LOGIN"
                                delay(3000)
                                supportActionBar?.title = "$oldName"
                            }
                        }
                        name.toLowerCase() == "logout" -> {
                            supportActionBar?.title = "$oldName LOGOUT"
                        }
                        else -> {
                            viewModel.updateItem(id.toInt(), name)
                            binding.listOfMock.adapter?.notifyDataSetChanged()
                            supportActionBar?.title = user
                            viewModel.sendMessage("$user: " + message.toString())
                        }
                    }
                }
            }
        })

    }

    override fun onDestroy() {
        user?.let {
            viewModel.sendMessage("$it çıkış yaptı")
        }
        viewModel.closeWebsocket()
        super.onDestroy()
    }
}
