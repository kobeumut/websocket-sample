package com.umutadali.betbull.api

import com.umutadali.betbull.model.ResponseData
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

    @GET("emredirican/mock-api/db")
    fun getInformations(): Call<ResponseData>

}