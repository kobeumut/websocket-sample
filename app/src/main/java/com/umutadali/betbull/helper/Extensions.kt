package com.umutadali.betbull.helper

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.Activity
import android.app.Dialog
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.view.animation.LinearInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.SeekBar
import android.widget.Toast
import androidx.annotation.AnyRes
import androidx.annotation.RequiresApi
import androidx.appcompat.view.ContextThemeWrapper
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Job
import org.jetbrains.anko.activityManager
import org.jetbrains.anko.windowManager
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.security.SecureRandom
import kotlin.experimental.and


/**
 * Created by Umut ADALI
 */


fun String.checkInteger():Boolean{
    return try{
        this.toInt()
        true
    }catch(e:NumberFormatException){
        false
    }
}

fun Disposable.safelyDispose() {
    if (!this.isDisposed) {
        this.dispose()
    }
}

fun Dialog.safelyDismiss() {
    if (this.isShowing) {
        this.dismiss()
    }
}


fun Any?.shakeAnimator(propertyName: String) =
    ObjectAnimator.ofFloat(this, propertyName, 0f, 5f).apply {
        repeatMode = ValueAnimator.REVERSE
        repeatCount = 7
        duration = 50
        interpolator = LinearInterpolator()
    }



inline fun <reified T> Call<T>.enqueue(crossinline result: (Result<T>) -> Unit) {
    enqueue(object : Callback<T> {
        override fun onFailure(call: Call<T>, error: Throwable) {
            result(Result.failure(error))
        }

        override fun onResponse(call: Call<T>, response: Response<T>) {
            val data = response.body()
            if (data != null) {
                result(Result.success(data))
            }else result(Result.failure(Exception("data comes null")))
        }
    })
}