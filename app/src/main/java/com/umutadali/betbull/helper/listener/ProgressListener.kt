package com.umutadali.betbull.helper.listener

import android.content.Context
import androidx.lifecycle.LifecycleRegistry
import com.umutadali.betbull.helper.dialog.ProgressDialogModule
import org.koin.core.context.GlobalContext.get
import java.lang.ref.WeakReference

/**
 * @author Umut ADALI
 *
 * This class get a singleton progress dialog. Listener provides you show or dismiss progress dialog.
 * You need to use, just implement it. If you extend any base class and using this listener, hide process
 * is not required, it's done with lifecycleregistry in [ProgressDialogModule]
 *
 * @see [ProgressDialogModule]
 *
 */
interface ProgressListener {
    /**
     * This progress for only set config in [ProgressDialogModule]. You can change some custom properties
     * in this function
     */
    val progress: ProgressDialogModule
        get() = get().koin.get()

    /**
     * Simple way to show progress dialog
     *
     * @param lifecycleRegistry is not required. Retrieving over context in extended Base class.
     * We recommend set lifecycleRegistry if you don't extend any base classes
     * @receiver it must be activity [Context]
     */
    fun Context.showProgress(lifecycleRegistry: LifecycleRegistry? = null) {
        progress.show(WeakReference(this))
    }

    /**
     * Simple way to hide progress dialog
     *
     */
    fun hideProgress() {
        progress.dismiss()
    }

}