package com.umutadali.betbull.model

data class ResponseData(val data:MutableList<ResponseObject>)
data class ResponseObject(val id : Int, var name : String)